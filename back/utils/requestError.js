class RequestError extends Error {
  constructor(code, message = 'Something went wrong! Try again later!') {
    super(message);
    this.message = message;
    this.code = code;
  }

  static responseMessage(message = 'bad request', code = 400) {
    return new RequestError(code, message);
  }
}

module.exports = RequestError;
