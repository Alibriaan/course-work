const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const path = require('path');
const sequelize = require('./services/db');
const routes = require('./router');
const endErrorMiddleware = require('./middleware/requestError');
const session = require('express-session');
const fileUpload = require('express-fileupload');

const { DB_SESSION_KEY, DB_SESSION_SECRET } = require('./configuration/db');
const sessionStore = require('./services/session');
const app = express();
const PORT = process.env.PORT || 3000;

const corsOptions = {
  origin: ['http://localhost:8080', 'http://192.168.1.2:8080'],
  optionsSuccessStatus: 200,
  credentials: true,
  exposedHeaders: ['set-cookie'],
};

app
  .use(express.static('public'))
  .use('/img', express.static('/public/img'))
  .use(fileUpload({
    createParentPath: true,
  }))
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({extended: true}))
  .use(cookieParser())
  .use(session({
    key: DB_SESSION_KEY,
    secret: DB_SESSION_SECRET,
    cookie: {
      maxAge: 1000 * 60 * 60 * 1,
      secure: false,
    },
    store: sessionStore,
    resave: false,
    saveUninitialized: false,
    unset: 'destroy',
  }))
  .use(cors(corsOptions))
  .use('/', routes)
  .use(endErrorMiddleware);

(async function() {
  try {
    await sequelize.sync();

    app.listen(PORT, () => {
      console.log(`Listen in ${PORT}`);
    });
  } catch (err) {
    console.log(err.message);
  }
})()