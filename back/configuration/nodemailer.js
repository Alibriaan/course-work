module.exports = {
  HOST: process.env.NODE_ENV !== "production" ? 'smtp.gmail.com' : process.env.HOST,
  PORT: process.env.NODE_ENV !== "production" ? 587 : process.env.PORT,
  SECURE: process.env.NODE_ENV !== "production" ? false : process.env.SECURE,
  USER: process.env.NODE_ENV !== "production" ? "nodemailersobolevskyi@gmail.com" : process.env.USER,
  PASS: process.env.NODE_ENV !== "production" ? 'passForNodemailer' : process.env.PASS,
  FROM: process.env.NODE_ENV !== "production" ? "Sobolevskyi <nodemailersobolevskyi@gmail.com>" : process.env.FROM,
}