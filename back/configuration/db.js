module.exports = {
  DB_NAME: process.env.NODE_ENV !== "production" ? 'pictureGallery' : process.env.DB_NAME,
  DB_USERNAME: process.env.NODE_ENV !== "production" ? 'root' : process.env.DB_USERNAME,
  DB_PASSWORD: process.env.NODE_ENV !== "production" ? 'PasswordForRoot' : process.env.DB_PASSWORD,
  DB_HOST: process.env.NODE_ENV !== "production" ? 'localhost' : process.env.DB_HOST,
  DB_DIALECT: process.env.NODE_ENV !== "production" ? 'mysql' : process.env.DB_DIALECT,
  DB_PORT: process.env.NODE_ENV !== "production" ? 3306 : process.env.DB_PORT,
  DB_SESSION_KEY: process.env.NODE_ENV !== "production" ? 'sessionKeyForDbSEssion' : process.env.DB_SESSION_KEY,
  DB_SESSION_SECRET: process.env.NODE_ENV !== "production" ? 'secretKeyForDbSEssion' : process.env.DB_SESSION_SECRET,
}