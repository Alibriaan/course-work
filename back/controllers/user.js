const { User, Role, Permission, PermissionType } = require('../db/models');
const bcrypt = require('bcrypt');
const RequestError = require('../utils/requestError');
const authCode = require('../services/registrationConfirmation');
const { transporter, generateVerificationMailConfig } = require('../services/nodemailer');

const getUsers = async (req, res) => {
  try {
    const result = await User.findAll();

    res.status(200).json(result);
  } catch (err) {
    console.log(err.message);
    res.status(400).json('get users error');
  }
}

const getUserById = async (req, res) => {
  try {
    const id = +req.params.id;
    const result = await User.findByPk(id);

    res.status(200).json(result);
  } catch (err) {
    console.log(err);
    res.status(err.code || 400).json({ message: err.message || 'something wrong'});
  }
}

const registrationBeforeConfirmation = async (req, res) => {
  try {
    const creds = { ...req.body };
    const result  = await User.findOne({
      where: {
        login: req.body.login,
        password: req.body.password,
      },
      returning: true,
      plain: true,
    });

    if(!!result) throw RequestError.responseMessage('user exist', 400);

    creds.password = bcrypt.hashSync(req.body.password, 10);

    req.session.cookie.maxAge = 1000 * 60 * 5;
    req.session.verificationCode = authCode();
    req.session.verificateCreds = creds;

    const emailMessage = generateVerificationMailConfig(
      req.session.verificateCreds.login,
      {
        verificationCode: req.session.verificationCode,
      }
    );

    const sendResult = await transporter.sendMail(emailMessage);

    res.status(200).json('verificate');
  } catch (err) {
    console.log(err);
    res.status(err.status || 400).send(err || 'something wrong');
  }
}

const refreshConfirmation = async(req, res) => {
  try {
    if(req.session.verificationCode && req.session.verificateCreds) {
      req.session.cookie.maxAge = 1000 * 60 * 5;
      req.session.verificationCode = authCode();
      req.session.verificateCreds = creds;

      const emailMessage = generateVerificationMailConfig(
        req.session.verificateCreds.email,
        {
          verificationCode: req.session.verificationCode,
        }
      );
      const sendResult = await transporter.sendMail(emailMessage);

      if(sendResult instanceof Error) throw sendResult

    } else {
      throw RequestError.responseMessage('Verification timeout', 400)
    }
  } catch(err) {
    res.status(err.status || 400).send(err.message || 'something wrong');
  }
}

const confirmateUser = async (req, res) => {
  try {
    if(!req.session.verificationCode || !req.session.verificateCreds ) throw RequestError.responseMessage('Verification timeout', 400);

    const user  = await User.findOne({
      where: {
        login: req.session.verificateCreds.login,
        password: req.session.verificateCreds.password,
      },
      returning: true,
      plain: true,
    });

    if(user) throw RequestError.responseMessage('User already registered', 400);
    if(req.body.verificationCode !== req.session.verificationCode) throw RequestError.responseMessage('Invalid number code', 400);

    if(req.body.verificationCode === req.session.verificationCode) {
      const userRole = await Role.findOne({
        where: {
          name: 'user',
        }
      });

      if(!userRole) throw RequestError.responseMessage('Create error', 400);

      await User.create({
        ...req.session.verificateCreds,
        roleId: userRole.dataValues.id,
      });

      res.status(200).json({ message: 'success created' });
    }
  } catch(err) {
    res.status( err.code || 400).json({ message: err.message || 'something wrong' });
  }
}

const validateUser = async (req, res) => {
  try {
    const result  = await await User.findOne({
      include: [
        {
          model: Role,
          attributes: {exclude: ['id']},
          include: [
            {
              model: Permission,
              as: 'permissions',
              attributes: {exclude: ['id', 'permissionTypeId']},
              include: [{
                model: PermissionType,
                attributes: {exclude: ['id']},
              }],
            },
          ],
        },
      ],
      attributes: {exclude: ['id', 'roleId']},
      where: { login: req.body.login},
    });

    if(!result) throw RequestError.responseMessage('Invalid user name', 400);

    const compareResult = bcrypt.compareSync(req.body.password, result?.password);
    const displayPermissions = result.role.permissions.filter((permission) => permission.permissionType.name  === 'access').map((permission) => permission.name);
    const crudPermissions = result.role.permissions.filter((permission) => permission.permissionType.name  === 'crud').map((permission) => permission.name);;

    if(compareResult) {
      req.session.isAuthorized = true;
      req.session.user = result;
      req.session.permissions = displayPermissions;
      req.session.crud = crudPermissions;
      req.session.id = result.id;

      res.status(200).json({
        permissions: displayPermissions,
      });
    } else if(!compareResult){
      throw RequestError.responseMessage('Invalid password', 400);
    }
  } catch (err) {
    if(err?.original?.code === "ER_DUP_ENTRY") {
      res.status(400).json({ message: 'user exist' });
    } else {
      res.status(err.status || 400).json({ message: err.message || 'something wrong' });
    }
  }
}

const checkUserAuthorizatedStatus = (req, res) => {
  if(req.session.isAuthorized) {
    res.status(200).json({
      isAuthorized: req.session.isAuthorized,
      permissions: req.session.displayPermissions,
    })
  } else {
    res.status(400).json('User session doesn\'t exist');
  }
}

const updateUser = async (req, res) => {
  try {
    const id = +req.params.id;
    const result =  await User.update(req.body, {
      where: { id: +id },
      returning: true,
      plain: true
    });

    return res.status(200).json(result);
  } catch (err) {
    console.log(err.message);
  }


}

const removeUser = async (req, res) => {
  try {
    const id = +req.params.id;

    await User.destroy({
      where: { id }
    });

    return res.status(200).text('User removed');
  } catch (err) {
    console.log(err.message);
  }
}

const logOut = (req, res) => {
  try {
    req.session.destroy();
    res.status(200).send('log-out');
  } catch (err) {
    console.log(err);
    res.status(err.statusCode || 400).json(err.message || 'something wrong');
  }
}

module.exports = {
  getUsers,
  getUserById,
  validateUser,
  registrationBeforeConfirmation,
  refreshConfirmation,
  checkUserAuthorizatedStatus,
  confirmateUser,
  updateUser,
  removeUser,
  logOut,
}
