const RequestError = require('../utils/requestError');
const path = require('path');
const fs = require('fs');
const sequelize = require('../services/db');

const getImgTypes = async (req, res) => {
  try {
    const typeDirectory = fs.readdirSync(path.join(process.cwd(), 'public', 'img'));
    const result = typeDirectory.filter((item) => item !== '.DS_Store')

    res.status(200).json(result);
  } catch (err) {
    res.status(err.status || 400).json(err.message || 'something wrong');
  }
};

const getAllImgPath = async (req, res) => {
  try {
    const serverUrl = req.headers.host;
    const typeDirectory = fs.readdirSync(path.join(process.cwd(), 'public', 'img'));
    const typeStatus = typeDirectory.find((item) => item === req.params.type);

    if(typeStatus) {
      const directoryContent = fs.readdirSync(path.join(process.cwd(), 'public', 'img', req.params.type));
      const result = directoryContent
        .filter((item) => item !== '.DS_Store')
        .map((item) => path.join(serverUrl, 'img', req.params.type, item));

      res.status(200).json(result);
    } else {
      res.status(400).json('Invalid type');
    }
  } catch (err) {
    res.status(err.status || 400).json(err.message || 'something wrong');
  }
}

const addNewImg = async (req, res) => {
  try {
    console.log(req.files.file);
    if(req.files) {
      await req.files.file.mv(path.resolve(`./public/img/${req.params.type}/${req.files.file.name}`));

      res.status(200).json('success add img');
    } else {
      throw RequestError.responseMessage('upload error', 400);
    }

  } catch (err) {
    res.status(err.status || 400).json(err.message || 'something wrong');
  }

}

const getAllSchemas = async (req, res) => {
  try {
    const response = Object.keys(sequelize.models);

    res.status(200).json(response);
  } catch (err) {
    res.status(err.status || 400).json(err.message || 'something wrong');
  }
}


const getSchemaModel = async (req, res) => {
  try {
    const response = await sequelize.models[req.params.model].fieldRawAttributesMap;

    console.log(sequelize.models);

    res.status(200).json(response);
  } catch (err) {
    res.status(err.status || 400).json(err.message || 'something wrong');
  }
}

const getSchemaValues = async (req, res) => {
  try {
    const response = await sequelize.models[req.params.model].findAll();

    res.status(200).json(response);
  } catch (err) {
    res.status(err.status || 400).json(err.message || 'something wrong');
  }
}

const addSchemaValue = async (req, res) => {
  try {
    const response = await sequelize.models[req.params.model].create(req.body);

    res.status(200).json(response);
  } catch (err) {
    res.status(err.status || 400).json(err.message || 'something wrong');
  }
}

const editSchemaValue = async (req, res) => {
  try {
    console.log(req.body);

    const response = await sequelize.models[req.params.model].update(req.body.newValue, {
      where: req.body.oldValue,
      limit: 1,
    });

    res.status(200).json(response);
  } catch (err) {
    res.status(err.status || 400).json(err.message || 'something wrong');
  }
}

const removeSchemaValue = async (req, res) => {
  try {
    const response = await sequelize.models[req.params.model].destroy({
      where: req.query,
      limit: 1,
    });

    res.status(200).json(response);
  } catch (err) {
    res.status(err.status || 400).json(err.message || 'something wrong');
  }
}

module.exports = {
  getImgTypes,
  getAllImgPath,
  getAllSchemas,
  getSchemaModel,
  getSchemaValues,
  addSchemaValue,
  editSchemaValue,
  removeSchemaValue,
  addNewImg,
}
