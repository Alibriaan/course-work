const { Picture, Genre, DrawTechnic, Author } = require('../db/models');

const getPictures = async (req, res, next) => {
  try {
    const page = +req.query.page || 1;
    const limit = +req.query.limit || 10;

    const result = await Picture.findAll({
      include: [{
        model: Genre,
      },
    {
      model: DrawTechnic,
    }, {
      model: Author,
    }],
      offset: ((page - 1) * limit),
      limit : limit,
    });

    res.status(200).json(result);
  } catch (err) {
    res.status( err.code || 400).json({ message: err.message || 'something wrong' });
  }
}

const getPictureById = async (req, res, next) => {
  try {
    const id = +req.params.id;
    const result = await Picture.findByPk(id, {
      include: [{
        model: Genre,
      },
      {
        model: DrawTechnic,
      }, {
        model: Author,
      }],
    });

    res.status(200).json(result);
  } catch (err) {
    console.log(err.message);
  }
}

const getPicturesCount = async (req, res) => {
  try {
    const result = await Picture.count({});

    res.status(200).json(result);
  } catch (err) {
    res.status( err.code || 400).json({ message: err.message || 'something wrong' });
  }
}

module.exports = {
  getPictures,
  getPictureById,
  getPicturesCount,
}