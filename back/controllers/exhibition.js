const { Exhibition, Picture } = require('../db/models');

const getExhibitions = async (req, res, next) => {
  try {
    const page = +req.query.page || 1;
    const limit = +req.query.limit || 10;

    const result = await Exhibition.findAll({
      include: [{
        model: Picture,
      }],
      offset: ((page - 1) * limit),
      limit : limit,
    });

    res.status(200).json(result);
  } catch (err) {
    console.log(err.message);
  }
}

const getExhibitionById = async (req, res, next) => {
  try {
    const id = +req.params.id;
    const page = +req.query.page || 1;
    const limit = +req.query.limit || 10;

    const result = await Exhibition.findByPk(id, {
      include: [{
        model: Picture,
      }],
      offset: ((page - 1) * limit),
      limit : limit,
    });

    res.status(200).json(result);
  } catch (err) {
    console.log(err.message);
  }
}

const getExhibitionsCount = async (req, res, next) => {
  try {
    const result = await Exhibition.count({});

    res.status(200).json(result);
  } catch (err) {
    res.status( err.code || 400).json({ message: err.message || 'something wrong' });
  }
}

const getCountOfExhibitionImagesById = async (req, res) => {
  try {
    const result = await Picture.count({
      include: [{
        model: Exhibition,
        where: { id: req.params.id}
      }],
    });

    res.status(200).json(result);
  } catch (err) {
    res.status( err.code || 400).json({ message: err.message || 'something wrong' });
  }
}
module.exports = {
  getExhibitions,
  getExhibitionById,
  getExhibitionsCount,
  getCountOfExhibitionImagesById,
}