const { DrawTechnic } = require('../db/models');

const getDrawTechnics = async (req, res, next) => {
  try {
    const result = await DrawTechnic.findAll();

    res.status(200).json(result);
  } catch (err) {
    console.log(err.message);
  }
}

const getDrawTechnicById = async (req, res, next) => {
  try {
    const id = +req.params.id;
    const result = await DrawTechnic.findByPk(id);

    res.status(200).json(result);
  } catch (err) {
    console.log(err.message);
  }
}

module.exports = {
  getDrawTechnics,
  getDrawTechnicById,
}