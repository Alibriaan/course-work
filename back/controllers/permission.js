const { Permission } = require('../db/models');

const getPermissions = async (req, res, next) => {
  try {
    const result = await Permission.findAll();

    res.status(200).json(result);
  } catch (err) {
    console.log(err.message);
  }
}

const getPermissionById = async (req, res, next) => {
  try {
    const id = +req.params.id;
    const result = await Permission.findByPk(id);

    res.status(200).json(result);
  } catch (err) {
    console.log(err.message);
  }
}

module.exports = {
  getPermissions,
  getPermissionById,
}