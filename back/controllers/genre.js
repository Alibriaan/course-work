const { Genre } = require('../db/models');

const getGenres = async (req, res, next) => {
  try {
    const result = await Genre.findAll();

    res.status(200).json(result);
  } catch (err) {
    console.log(err.message);
  }
}

const getGenreById = async (req, res, next) => {
  try {
    const id = +req.params.id;
    const result = await Genre.findByPk(id);

    res.status(200).json(result);
  } catch (err) {
    console.log(err.message);
  }
}

module.exports = {
  getGenres,
  getGenreById,
}