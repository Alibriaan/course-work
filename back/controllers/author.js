const { Author } = require('../db/models');

const getAuthors = async (req, res, next) => {
  try {
    const page = +req.query.page || 1;
    const limit = +req.query.limit || 10;

    console.log(page, limit);
    const result = await Author.findAll({
      offset: ((page - 1) * limit),
      limit : limit,
    });

    res.status(200).json(result);
  } catch (err) {
    console.log(err.message);
  }
}

const getAuthorById = async (req, res, next) => {
  try {
    const id = +req.params.id;
    const result = await Author.findByPk(id);

    res.status(200).json(result);
  } catch (err) {
    console.log(err.message);
  }
}

const getAuthorsCount = async (req, res, next) => {
  try {
    const result = await Author.count({});

    res.status(200).json(result);
  } catch (err) {
    res.status( err.code || 400).json({ message: err.message || 'something wrong' });
  }
}

module.exports = {
  getAuthors,
  getAuthorById,
  getAuthorsCount,
}