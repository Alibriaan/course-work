const RequestError = require('../utils/requestError');

const permissionGuard = (perrmisionList) => (req, res, next) => {
  for(let permissionObj of perrmisionList) {
    if(!req.session[permissionObj.type].includes(permissionObj.name)) {
      return next(RequestError.responseMessage('Request body cannot contain  permission', 400));
    }
  }

  next();
};

module.exports = permissionGuard;