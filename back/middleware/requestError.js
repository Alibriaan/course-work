const endErrorMiddleware = (error, req, res, next) => {
  console.log(error.message, error.code);
  res.status(error.code || 500).send();
};

module.exports = endErrorMiddleware;