const RequestError = require('../utils/requestError');

const authenticationMiddleware = () => {
  return function (req, res, next) {
      if (req.isAuthenticated()) {
          return next()
      } else {
        return next(RequestError.responseMessage(400, 'User cannot authorizate'));
      }
  }
}

module.exports = authenticationMiddleware;