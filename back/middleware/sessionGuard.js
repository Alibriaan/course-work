const sessionGuard = (req, res, next) => {
  if(req.session.isAuthorized) {
    next();
  } else {
    res.status(400).json('session timeout');
  }
};

module.exports = sessionGuard;