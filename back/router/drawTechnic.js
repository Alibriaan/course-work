const express = require('express');
const router = express.Router();
const { getDrawTechnics, getDrawTechnicById  } = require('../controllers/drawTechnic');

router.get('/all', getDrawTechnics);

router.get('/:id', getDrawTechnicById);

module.exports = router;
