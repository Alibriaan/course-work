const express = require('express');
const router = express.Router();
const { getPictures, getPictureById, getPicturesCount  } = require('../controllers/picture');
const { body, param } = require('express-validator');
const requestDataGuard = require('../middleware/requestDataGuard');
const permissionGuard = require('../middleware/permissionGuard');
const sessionGuard = require('../middleware/sessionGuard');

router.get('/all', getPictures);

router.get('/count', getPicturesCount);

router.get('/:id', getPictureById);


module.exports = router;
