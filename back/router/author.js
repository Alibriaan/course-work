const express = require('express');
const router = express.Router();
const { getAuthors, getAuthorById, getAuthorsCount } = require('../controllers/author');

router.get('/all', getAuthors);

router.get('/count', getAuthorsCount);

router.get('/:id', getAuthorById);

module.exports = router;
