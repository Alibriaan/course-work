const express = require('express');
const router = express.Router();
const { getPermissions, getPermissionById  } = require('../controllers/permission');
const { param } = require('express-validator');

router.get('/all', getPermissions);

const sequelize = require('../services/db');

router.get(
  '/:id',
  param('id').notEmpty().isString(),
  getPermissionById
);

module.exports = router;
