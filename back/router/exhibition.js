const express = require('express');
const router = express.Router();
const { getExhibitions, getExhibitionsCount, getExhibitionById, getCountOfExhibitionImagesById } = require('../controllers/exhibition');

router.get('/all', getExhibitions);

router.get('/count', getExhibitionsCount);

router.get('/:id/img/count', getCountOfExhibitionImagesById)

router.get('/:id', getExhibitionById);

module.exports = router;
