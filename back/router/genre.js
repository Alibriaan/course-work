const express = require('express');
const router = express.Router();
const { getGenres, getGenreById  } = require('../controllers/genre');

router.get('/all', getGenres);

router.get('/:id', getGenreById);

module.exports = router;
