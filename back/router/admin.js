const express = require('express');
const router = express.Router();
const {
  getImgTypes,
  getAllImgPath,
  addNewImg,
  getAllSchemas,
  getSchemaModel,
  getSchemaValues,
  addSchemaValue,
  editSchemaValue,
  removeSchemaValue
} = require('../controllers/admin');
const permissionGuard = require('../middleware/permissionGuard');
const sessionGuard = require('../middleware/sessionGuard');

router.get( '/img-types',
  sessionGuard,
  permissionGuard([
    {
      type: 'crud',
      name: 'view'
    }]
  ),
  getImgTypes
);

router.get(
  '/img-assets/:type',
  sessionGuard,
  permissionGuard([
    {
      type: 'crud',
      name: 'view'
    },
  ]),
  getAllImgPath
)

router.post(
  '/add-img/:type',
  sessionGuard,
  permissionGuard([{
      type: 'crud',
      name: 'edit'
    }]
  ),
  addNewImg,
);

router.get(
  '/schema/all',
  sessionGuard,
  permissionGuard([{
      type: 'crud',
      name: 'view'
    }]
  ),
  getAllSchemas,
);

router.get(
  '/schema/model/:model',
  sessionGuard,
  permissionGuard([
    {
      type: 'crud',
      name: 'view'
    }]
  ),
  getSchemaModel
)

router.get(
  '/schema/value/:model',
  sessionGuard,
  permissionGuard([
    {
      type: 'crud',
      name: 'view'
    }]
  ),
  getSchemaValues
)

router.post(
  '/schema/add/:model',
  sessionGuard,
  permissionGuard([
    {
      type: 'crud',
      name: 'edit'
    }]
  ),
  addSchemaValue
)

router.put(
  '/schema/edit/:model',
  sessionGuard,
  permissionGuard([
    {
      type: 'crud',
      name: 'edit'
    }]
  ),
  editSchemaValue
)

router.delete(
  '/schema/delete/:model',
  sessionGuard,
  permissionGuard([
    {
      type: 'crud',
      name: 'remove'
    }]
  ),
  removeSchemaValue
)

module.exports = router;
