const express = require('express');
const router = express.Router();
const { getUsers, getUserById, validateUser, registrationBeforeConfirmation, refreshConfirmation, confirmateUser, updateUser, removeUser, logOut, checkUserAuthorizatedStatus  } = require('../controllers/user');
const { body, param } = require('express-validator');
const requestDataGuard = require('../middleware/requestDataGuard');
const permissionGuard = require('../middleware/permissionGuard');
const sessionGuard = require('../middleware/sessionGuard');

router.get('/all', getUsers);

router.post(
  '/create',
  body('login').isString(),
  body('password').isString(),
  requestDataGuard,
  registrationBeforeConfirmation,
);

router.post(
  '/auth/confirmation-refresh',
  refreshConfirmation,
);

router.post(
  '/auth/confirmate-registration',
  body('verificationCode').isString(),
  requestDataGuard,
  confirmateUser,
);

router.post(
  '/auth/validate',
  body('login').isString(),
  body('password').isString(),
  requestDataGuard,
  validateUser
);

router.get(
  '/auth/is-authorizated',
  checkUserAuthorizatedStatus,
)

router.get(
  '/auth/logout',
  logOut,
);

router.get(
  '/:id',
  sessionGuard,
  getUserById
);

router.put(
  '/:id',
  param('id').isString(),
  requestDataGuard,
  sessionGuard,
  permissionGuard([{
    type: 'crud',
    name: 'edit',
  }]),
  updateUser
);

router.delete(
  '/:id',
  param('id').isString(),
  requestDataGuard,
  sessionGuard,
  permissionGuard([{
    type: 'crud',
    name: 'remove',
  }]),
  removeUser
);

module.exports = router;
