const express = require('express');
const router = express.Router();
const models = require('../db/models');

const author = require('./author');
const drawTechnic = require('./drawTechnic');
const genre = require('./genre');
const permission = require('./permission');
const picture = require('./picture');
const user = require('./user');
const admin = require('./admin');
const exhibition = require('./exhibition');

router.get('/', (req, res) => {
  res.send('Server is working');
})

router.post('/', (req, res) => {
  res.send('hello world');
})

router
  .use('/admin', admin)
  .use('/author', author)
  .use('/drawTechnic', drawTechnic)
  .use('/genre', genre)
  .use('/permission', permission)
  .use('/picture', picture)
  .use('/exhibition', exhibition)
  .use('/user', user);

module.exports = router;
