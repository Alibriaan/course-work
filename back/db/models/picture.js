const Sequelize = require('sequelize');
const sequelize = require('../../services/db');

const Picture = sequelize.define('pictures', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  name: {
    allowNull: false,
    type: Sequelize.STRING,
    unique: true,
  },
  description: {
    allowNull: false,
    type: Sequelize.STRING,
  },
  createDate: {
    allowNull: true,
    type: Sequelize.DATE,
  },
  authorCode: {
    allowNull: true,
    type: Sequelize.INTEGER,
  },
  genreCode: {
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  drawTechnicCode: {
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  imgPath: {
    allowNull: false,
    type: Sequelize.STRING,
  },
}, {
  timestamps: false,
});

module.exports = Picture;