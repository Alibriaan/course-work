const Sequelize = require('sequelize');
const sequelize = require('../../services/db');

const Permission = sequelize.define('permissions', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  name: {
    allowNull: false,
    type: Sequelize.STRING,
    unique: true,
  },
  permissionTypeId: {
    allowNull: false,
    type: Sequelize.INTEGER,
  }
}, {
  timestamps: false,
});

module.exports = Permission;