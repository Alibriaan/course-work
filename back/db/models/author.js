const Sequelize = require('sequelize');
const sequelize = require('../../services/db');

const Author = sequelize.define('authors', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  name: {
    allowNull: false,
    type: Sequelize.STRING,
    unique: true,
  },
  birthDate: {
    allowNull: false,
    type: Sequelize.DATE,
  },
  description: {
    allowNull: false,
    type: Sequelize.STRING,
  },
  imgPath: {
    allowNull: false,
    type: Sequelize.STRING,
  },
}, {
  timestamps: false,
});

module.exports = Author;