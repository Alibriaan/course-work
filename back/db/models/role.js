const Sequelize = require('sequelize');
const sequelize = require('../../services/db');

const Role = sequelize.define('roles', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  name: {
    allowNull: false,
    type: Sequelize.STRING,
    unique: true,
  },
},  {
  timestamps: false,
});

module.exports = Role;