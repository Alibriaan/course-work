const Sequelize = require('sequelize');
const sequelize = require('../../services/db');

const PermissionType = sequelize.define('permissionTypes', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  name: {
    allowNull: false,
    type: Sequelize.STRING,
    unique: true,
  },
}, {
  timestamps: false,
});

module.exports = PermissionType;