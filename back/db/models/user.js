const Sequelize = require('sequelize');
const sequelize = require('../../services/db');

const User = sequelize.define('users', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  login: {
    allowNull: false,
    type: Sequelize.STRING,
    unique: true,
  },
  password: {
    allowNull: false,
    type: Sequelize.STRING,
  },
  roleId: {
    allowNull: false,
    type: Sequelize.INTEGER,
  },
}, {
  timestamps: false,
});

module.exports = User;