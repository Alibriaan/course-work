const Picture = require('./picture');
const Genre = require('./genre');
const Author = require('./author');
const DrawTechnic = require('./drawTechnic');
const User = require('./user');
const Permission = require('./permission');
const Role = require('./role');
const PermissionType = require('./permissionType');
const Exhibition = require('./exhibition');

Genre.hasMany(Picture, { foreignKey: 'genreCode', });
Author.hasMany(Picture, { foreignKey: 'authorCode', });
DrawTechnic.hasMany(Picture, { foreignKey: 'drawTechnicCode',});
Picture.belongsTo(Genre, { foreignKey: 'genreCode' });
Picture.belongsTo(Author, { foreignKey: 'authorCode'});
Picture.belongsTo(DrawTechnic, { foreignKey: 'drawTechnicCode' });

Role.hasMany(User, { foreignKey: 'roleId'});
User.belongsTo(Role);

PermissionType.hasMany(Permission, { foreignKey: 'permissionTypeId'})

Permission.belongsTo(PermissionType);

Exhibition.belongsToMany(Picture, {
  through: 'exhibitionsPictures',
  foreignKey: 'exhibitionId',
  timestamps: false,
})

Picture.belongsToMany(Exhibition, {
  through: 'exhibitionsPictures',
  foreignKey: 'pictureId',
  timestamps: false,
})

Role.belongsToMany(Permission, {
  through: 'rolePermission',
  foreignKey: 'roleId',
  timestamps: false,
  as: 'permissions',
});

Permission.belongsToMany(Role, {
  through: 'rolePermission',
  foreignKey: 'permissionId',
  timestamps: false,
  as: 'roles',
});


module.exports = {
  Picture,
  Genre,
  Author,
  DrawTechnic,
  User,
  Permission,
  Exhibition,
  Role,
  PermissionType,
}