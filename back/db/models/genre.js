const Sequelize = require('sequelize');
const sequelize = require('../../services/db');

const Genre = sequelize.define('genres', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  name: {
    allowNull: false,
    type: Sequelize.STRING,
    unique: true,
  },
  description: {
    allowNull: false,
    type: Sequelize.STRING,
  },
  imgPath: {
    allowNull: false,
    type: Sequelize.STRING,
  },
}, {
  timestamps: false,
});

module.exports = Genre;