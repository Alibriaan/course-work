const Sequelize = require('sequelize');
const sequelize = require('../../services/db');

const Exhibitions = sequelize.define('exhibitions', {
  id: {
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  name: {
    allowNull: false,
    type: Sequelize.STRING,
  },
  description: {
    allowNull: true,
    type: Sequelize.STRING,
  },
  imgPath: {
    allowNull: true,
    type: Sequelize.STRING,
  },
}, {
  timestamps: false,
});

module.exports = Exhibitions;