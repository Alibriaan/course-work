const { DB_NAME, DB_USERNAME, DB_PASSWORD, DB_HOST, DB_DIALECT,} = require('../configuration/db');
const Sequelize = require('sequelize');

const sequelize = new Sequelize(DB_NAME, DB_USERNAME, DB_PASSWORD, {
  host: DB_HOST,
  dialect: DB_DIALECT,
});

module.exports = sequelize;