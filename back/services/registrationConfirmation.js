const authCode = (length = 10) => {
  return Math.random().toString(10).substring(2, length);
};

module.exports = authCode;