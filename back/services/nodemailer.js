const nodemailer = require('nodemailer');
const fs = require('fs');
const path = require('path');
const { HOST, PORT, SECURE, USER, PASS, FROM } = require('../configuration/nodemailer');
const RequestError = require('../utils/requestError');

const transporter = nodemailer.createTransport(
    {
        host: HOST,
        port: PORT,
        secure: SECURE,
        auth: {
            user: USER,
            pass: PASS,
        }
    },
    {
        from: FROM,
    }
);


const generateVerificationMailConfig = (sendEmail, templateData) => {
 try {
  let htmlTemplate = (fs.readFileSync(path.join(process.cwd(), 'emailTemplates', 'verification.html'), 'utf-8')).toString();

  Object.keys(templateData).forEach((key) => {
    htmlTemplate = htmlTemplate.replace(`|*${key}*|`, templateData[key]);
  });

  return {
    from: `Picture Gallery`,
    to: sendEmail,
    subject: "Verification",
    text: "Please confirm your email by this code (life time 5 minutes)",
    html: htmlTemplate,
  }
  } catch(err) {
    console.log(err.message);
    return RequestError.responseMessage('Email prepare error', 400);
  }
}
module.exports = {
  transporter,
  generateVerificationMailConfig,
};