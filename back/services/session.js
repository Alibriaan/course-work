const session = require('express-session');
const mysql = require('mysql');
const MySQLStore = require('express-mysql-session')(session);
const { DB_USERNAME, DB_PASSWORD, DB_NAME, DB_HOST, DB_PORT} = require('../configuration/db');
const options = {
	host: DB_HOST,
	port: DB_PORT,
	user: DB_USERNAME,
	password: DB_PASSWORD,
	database: DB_NAME,
};

const connection = mysql.createConnection(options);


const sessionStore = new MySQLStore({
  checkExpirationInterval: 900000,
  expiration: 10800000,
  createDatabaseTable: true,
  schema: {
    tableName: 'USERS_SESSIONS',
    columnNames: {
      session_id: 'session_id',
      expires: 'expires',
      data: 'data',
    }
  }
}, connection);

module.exports = sessionStore;