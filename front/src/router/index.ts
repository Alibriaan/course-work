import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '@/views/pages/content/Home.vue';
import GalleryList from '@/views/pages/content/GalleryList.vue';
import About from '@/views/pages/content/About.vue';
import AuthorizationRegistration from '@/views/pages/forms/AuthorizationRegistration.vue';
import Verification from '@/views/pages/forms/Verification.vue';
import AuthorsList from '@/views/pages/content/AuthorsList.vue';
import Exhibition from '@/views/pages/content/Exhibition.vue';
import AdminPanel from '@/views/pages/admin/AdminPanel.vue';
import AssetsManager from '@/views/pages/admin/AssetsManager.vue';
import CrudManager from '@/views/pages/admin/CrudManager.vue';
import Header from '@/components/Header.vue';
import Footer from '@/components/Footer.vue';

import store from '../store/index';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    components: {
      header: Header,
      default: Home,
      footer: Footer,
      test: Footer,
    },
  },
  {
    path: '/authorization',
    name: 'Authorization',
    component: AuthorizationRegistration,
  },
  {
    path: '/registration',
    name: 'Registration',
    component: AuthorizationRegistration,
  },
  {
    path: '/verification',
    name: 'Verification',
    component: Verification,
  },
  {
    path: '/picture-gallery',
    name: 'PictureGallery',
    components: {
      header: Header,
      default: GalleryList,
      footer: Footer,
    },
  },
  {
    path: '/authors-gallery',
    name: 'AuthorsList',
    components: {
      header: Header,
      default: AuthorsList,
      footer: Footer,
    },
  },
  {
    path: '/exhibition',
    name: 'Exhibition',
    components: {
      header: Header,
      default: Exhibition,
      footer: Footer,
    },
  },
  {
    path: '/admin-panel',
    name: 'AdminPanel',
    components: {
      default: AdminPanel,
    },
    meta: {
      protectedPage: true,
    },
    children: [
      {
        path: 'assets-manager',
        name: 'AssetsManager',
        components: {
          default: AssetsManager,
        },
      },
      {
        path: 'crud-tables/:schema',
        name: 'CrudManager',
        components: {
          default: CrudManager,
        },
      },
    ],
  },
  {
    path: '/about',
    name: 'About',
    component: About,
  },
];

const router = new VueRouter({
  routes,
});

router.beforeEach((to, from, next) => {
  console.log(from, to);
  const pageAccessAbility = store.getters['accessAbilityModule/getPageAccessStatusByPath'](to.path);

  if (pageAccessAbility) {
    next();
  } else {
    next('/authorization');
  }
});

export default router;
