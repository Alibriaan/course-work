export interface IPageItemRequest {
  page: number;
  limit: number;
}

export interface IUploadAssetsFile {
  type: string;
  file: FormData;
}
