import { IPageConfig } from './pageConfig.interface';

export interface IAccessModules {
    apiHost: string;
    isAuthorizated: boolean;
    permissionList: Array<string>;
    pageList: Array<IPageConfig>
}
