export interface IWindowStateModule {
  width: number,
  height: number,
  inputFocusStatus: boolean,
  orientation: string;
  userAgent: any;
  scrollHeight: number;
}
