export interface IPageConfig {
  name: string;
  link: string;
  permission?: string;
  hideInNavigation?: boolean;
}
