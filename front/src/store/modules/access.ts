/* eslint-disable import/prefer-default-export */
/* eslint-disable no-return-assign */
import {
  ActionTree,
  MutationTree,
  GetterTree,
  Module,
} from 'vuex';

import { IAccessModules } from '@/interfaces/IAccessModules.interface';
import { IRootState } from '@/interfaces/IRootState.interface';
import { IPageConfig } from '@/interfaces/pageConfig.interface';

const accessState: IAccessModules = {
  apiHost: process.env.NODE_ENV === 'production' ? '' : 'http://localhost:3000',
  isAuthorizated: false,
  permissionList: [],
  pageList: [{
    name: 'Главная',
    link: '/',
  },
  {
    name: 'Авторы',
    link: '/authors-gallery',
  },
  {
    name: 'Картины',
    link: '/picture-gallery',
  }, {
    name: 'Админ Панель',
    link: '/admin-panel',
    permission: 'adminPage',
  }, {
    name: 'Админ Панель',
    link: '/admin-panel/assets-manager',
    permission: 'adminPage',
    hideInNavigation: true,
  }],
};

const accessMutations: MutationTree<IAccessModules> = {
  setIsAuthorizated: (state: IAccessModules, payload: boolean) => state.isAuthorizated = payload,
  setPermissionList: (state: IAccessModules, payload: []) => state.permissionList = payload,
};

const accessActions: ActionTree<IAccessModules, IRootState> = {
  setIsAuthorizated: ({ commit }, payload: boolean) => commit('setIsAuthorizated', payload),
  setPermissionList: ({ commit }, payload: []) => commit('setPermissionList', payload),
};

const accessGetters: GetterTree<IAccessModules, IRootState> = {
  getIsAuthorizated: (state: IAccessModules) => state.isAuthorizated,
  getPermissionList: (state: IAccessModules) => state.permissionList,
  getPageList: (state: IAccessModules) => state.pageList,
  getAccessedPageList: (state: IAccessModules) => state.pageList.filter((page: IPageConfig) => {
    const isNotSecuredPage = state.isAuthorizated && !page.permission;
    const isAccessedPage = !page?.permission || state.permissionList.includes(page.permission);
    const isVisiblePage = !page.hideInNavigation;

    return (isNotSecuredPage || isAccessedPage) && isVisiblePage;
  }),
  getPageByPath: (state: IAccessModules) => (path: string) => state.pageList.find((item) => item.link === path),
  getPageAccessStatusByPath: (state: IAccessModules) => (path: string) => {
    const currentPage = state.pageList.find((item) => item.link === path);

    if (currentPage?.permission) {
      return state.isAuthorizated && state.permissionList.includes(currentPage.permission);
    }

    return true;
  },
  getApiHost: (state: IAccessModules) => state.apiHost,
};

const accessAbilityModule: Module<IAccessModules, IRootState> = {
  state: accessState,
  mutations: accessMutations,
  actions: accessActions,
  getters: accessGetters,
  namespaced: true,
};

export default accessAbilityModule;
