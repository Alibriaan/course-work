import {
  ActionTree,
  MutationTree,
  GetterTree,
  Module,
} from 'vuex';

import { IContentState } from '@/interfaces/IContentModules.interface';
import { IRootState } from '@/interfaces/IRootState.interface';

const contentState: IContentState = {
  content: [],
  genre: [],
  authors: [],
  drawTechnic: [],
  pictures: [],
};

const contentMutations: MutationTree<IContentState> = {
  setPeopleList: (state: IContentState, payload: []): null => null,
  setPeopleCounts: (state: IContentState, payload: []): null => null,
  setModuleType: (state: IContentState, module: []): null => null,
  setFilterObject: (state: IContentState, newFitlerObject: []): null => null,
};

const contentActions: ActionTree<IContentState, IRootState> = {
  setAllPeopleList: ({ commit }, requestPayload: []): null => null,
  setAllPeopleCounts: ({ commit }): null => null,
  setFilteredPeopleList: ({ commit, getters }, pageInformationObject?: []): null => null,
  setFilteredPeopleCounts: ({ commit, getters }): null => null,
  setPeopleModule: ({ commit }, moduleType: string): null => null,
  setFilterObject: ({ commit }, newFitlerObject: []): null => null,
};

const contentGetters: GetterTree<IContentState, IRootState> = {
  getAllPeople: (state: IContentState): null => null,
  getCountOfPeoples: (state: IContentState): null => null,
  getCurrentModule: (state: IContentState): null => null,
  getFilterObject: (state: IContentState): null => null,
};

const contentModule: Module<IContentState, IRootState> = {
  state: contentState,
  mutations: contentMutations,
  actions: contentActions,
  getters: contentGetters,
  namespaced: true,
};

export default contentModule;
