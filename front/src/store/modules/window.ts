/* eslint-disable */

import {
  ActionTree,
  MutationTree,
  GetterTree,
  Module,
} from 'vuex';

import { IWindowStateModule } from '@/interfaces/IWindowStateModule';
import { IRootState } from '@/interfaces/IRootState.interface';

const contentState: IWindowStateModule = {
  width: window.innerWidth,
  height: window.innerHeight,
  inputFocusStatus: false,
  orientation: window.matchMedia('(orientation: landscape)').matches ? 'landscape' : 'portrait',
  userAgent: navigator.userAgent,
  scrollHeight: 0,
};

const contentMutations: MutationTree<IWindowStateModule> = {
  setDeviceWidth: (state: IWindowStateModule, width: number) => state.width = width,
  setDeviceHeight: (state: IWindowStateModule, heihgt: number) => state.height = heihgt,
  setInputFocusStatus: (state: IWindowStateModule, status: boolean) => state.inputFocusStatus = status,
  setUserAgent: (state: IWindowStateModule, agent: any) => state.userAgent = agent,
  setDeviceOrientation: (state: IWindowStateModule, orientation: string) => state.orientation = orientation,
  setScrollHeight: (state: IWindowStateModule, scrollHeight: number) => state.scrollHeight = scrollHeight,
};

const contentActions: ActionTree<IWindowStateModule, IRootState> = {
  setDeviceWidth: ({ commit }, width: number) => commit('setDeviceWidth', width),
  setDeviceHeight: ({ commit }, heihgt: number) => commit('setDeviceHeight', heihgt),
  setInputFocusStatus: ({ commit }, status: boolean) => commit('setInputFocusStatus', status),
  setUserAgent: ({ commit }, agent: any) => commit('setUserAgent', agent),
  setDeviceOrientation: ({ commit }, orientation: string) => commit('setDeviceOrientation', orientation),
  setScrollHeight: ({ commit }, scrollHeight: number) => commit('setScrollHeight', scrollHeight),
};

const contentGetters: GetterTree<IWindowStateModule, IRootState> = {
  getDeviceWidth: (state: IWindowStateModule) => state.width,
  getDeviceHeight: (state: IWindowStateModule) => state.height,
  getInputFocusStatus: (state: IWindowStateModule) => state.inputFocusStatus,
  getScrollHeight: (state: IWindowStateModule) => state.scrollHeight,
  isLargeLaptopWidth: (state: IWindowStateModule) => state.width > 1024 && state.width <= 1440,
  isLaptopWidth: (state: IWindowStateModule) => state.width > 768 && state.width <= 1024,
  isTabletWidth: (state: IWindowStateModule) => state.width <= 768,
  isLargeMobileWidth: (state: IWindowStateModule) => state.width <= 425 && state.width > 375,
  isMediumMobileWidth: (state: IWindowStateModule) => state.width <= 375 && state.width < 320,
  isSmallMobileWidth: (state: IWindowStateModule) => state.width <= 320,
  isAndroid: (state: IWindowStateModule) => /Android/i.test(state.userAgent),
  isIos: (state: IWindowStateModule) => /iPhone|iPad|iPod/i.test(state.userAgent),
  isMacOs: (state: IWindowStateModule) => /Mac OS/i.test(state.userAgent),
  isWindows: (state: IWindowStateModule) => /asdsd/i.test(state.userAgent),
  isPortraitOrientation: (state: IWindowStateModule) => state.orientation === 'portrait',
  isLandscapeOrientation: (state: IWindowStateModule) => state.orientation === 'landscape',
};

const contentModule: Module<IWindowStateModule, IRootState> = {
  state: contentState,
  mutations: contentMutations,
  actions: contentActions,
  getters: contentGetters,
  namespaced: true,
};

export default contentModule;
