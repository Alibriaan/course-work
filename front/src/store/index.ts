/* eslint-disable max-len */
import Vue from 'vue';
import Vuex, {
  StoreOptions,
  ActionTree,
  MutationTree,
  GetterTree,
} from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import { IRootState } from '@/interfaces/IRootState.interface';
import contentModule from './modules/content';
import windowModule from './modules/window';
import accessAbilityModule from './modules/access';

Vue.use(Vuex);

const rootState: IRootState = {};

const rootMutations: MutationTree<IRootState> = {};

const rootActions: ActionTree<IRootState, IRootState> = {};

const rootGetters: GetterTree<IRootState, IRootState> = {};

const store: StoreOptions<IRootState> = {
  plugins: [createPersistedState()],
  state: rootState,
  mutations: rootMutations,
  actions: rootActions,
  getters: rootGetters,
  modules: {
    contentModule,
    windowModule,
    accessAbilityModule,
  },
};

export default new Vuex.Store<IRootState>(store);
