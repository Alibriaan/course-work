/* eslint-disable import/prefer-default-export */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { IUploadAssetsFile } from '@/interfaces/requests/index.interface';
import { AxiosApi } from '@/api/axios/index';

export const getImgTypes = () => AxiosApi.get('/admin/img-types');
export const getImg = (type: string) => AxiosApi.get(`/admin/img-assets/${type}`);
export const addImg = (path: string, data: any) => AxiosApi.post(`/admin/add-img/${path}`, data);

export const getSchemas = () => AxiosApi.get('/admin/schema/all');
export const getSchemaModel = (data: string) => AxiosApi.get(`/admin/schema/model/${data}`);
export const getSchemaValues = (data: string) => AxiosApi.get(`/admin/schema/value/${data}`);

export const addSchemaValue = (model: string, data: any) => AxiosApi.post(`/admin/schema/add/${model}`, data);
export const editSchemaValue = (model: string, oldValue: any, newValue: any) => AxiosApi.put(`/admin/schema/edit/${model}`, {
  oldValue,
  newValue,
});
export const removeSchemaValue = (model: string, queryParams: string) => AxiosApi.delete(`/admin/schema/delete/${model}?${queryParams}`);
