import { AxiosApi } from '@/api/axios/index';

export const getExhibitions = (page: number, limit: number) => AxiosApi.get(`/exhibition/all?page=${page}&limit=${limit}`);
export const getCountOfexhibition = () => AxiosApi.get('/exhibition/count');
export const getExhibitionById = (id: number, page: number, limit: number) => AxiosApi.get(`/exhibition/${id}?page=${page}&limit=${limit}`);
export const getCountOfExhibitionImagesById = (id: number) => AxiosApi.get(`/exhibition/${id}/img/count`);
