import { AxiosApi } from '@/api/axios/index';

export const getPictures = (page: number, limit: number) => AxiosApi.get(`picture/all?page=${page}&limit=${limit}`);
export const getCountOfPictures = () => AxiosApi.get('/picture/count');
export const getPictureById = (id: number) => AxiosApi.get(`picture/${id}`);
