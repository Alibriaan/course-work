/* eslint-disable import/prefer-default-export */
import { AxiosApi } from '@/api/axios/index';

export const getTechnics = () => AxiosApi.get('drawTechnic/all');
