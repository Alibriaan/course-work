/* eslint-disable import/prefer-default-export */
import { AxiosApi } from '@/api/axios/index';

export const getGenres = () => AxiosApi.get('genre/all');
