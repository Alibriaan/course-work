import { AxiosApi } from '@/api/axios/index';

export const getAuthors = (page: number, limit: number) => AxiosApi.get(`/author/all?page=${page}&limit=${limit}`);
export const getCountOfAuthors = () => AxiosApi.get('/author/count');
