/* eslint-disable import/prefer-default-export */
/* eslint-disable @typescript-eslint/no-explicit-any */
import axios from 'axios';

const configuration = {
  baseURL: process.env.NODE_ENV === 'production' ? '' : 'http://localhost:3000',
  mode: 'cors',
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  },
  withCredentials: true,
};

export const AxiosApi = axios.create(configuration);

AxiosApi.interceptors.response.use((res) => Promise.resolve(res),
  (error) => {
    console.log(error.response);
    if (error.response.data === 'session timeout') {
      window.location.hash = '/authorization';
    }

    return Promise.reject(error);
  });
