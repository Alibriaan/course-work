import { AxiosApi } from '@/api/axios/index';

const checkSession = () => AxiosApi.get('/user/check');

export default checkSession;
