import { AxiosApi } from '@/api/axios/index';

export const registerateUser = (data: any) => AxiosApi.post('/user/create', data);
export const validateCreds = (data: any) => AxiosApi.post('/user/auth/validate', data);
export const verificate = (data: any) => AxiosApi.post('/user/auth/confirmate-registration', data);
export const authorizationCheck = () => AxiosApi.get('/user/auth/is-authorizated');
export const logOut = () => AxiosApi.get('/user/auth/logout');
